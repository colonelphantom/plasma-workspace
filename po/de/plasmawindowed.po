# German translations for plasma-workspace package
# German translation for plasma-workspace.
# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-09 01:39+0000\n"
"PO-Revision-Date: 2023-09-26 13:41+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: main.cpp:44 plasmawindowedcorona.cpp:103
#, kde-format
msgid "Plasma Windowed"
msgstr ""

#: main.cpp:46
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr ""

#: main.cpp:49 plasmawindowedcorona.cpp:105
#, kde-format
msgid ""
"Makes the plasmoid stay alive in the Notification Area, even when the window "
"is closed."
msgstr ""

#: main.cpp:51
#, kde-format
msgid "Force loading the given shell plugin"
msgstr ""

#: main.cpp:55 plasmawindowedcorona.cpp:106
#, kde-format
msgid "The applet to open."
msgstr ""

#: main.cpp:56 plasmawindowedcorona.cpp:107
#, kde-format
msgid "Arguments to pass to the plasmoid."
msgstr ""

#: plasmawindowedview.cpp:120
#, kde-format
msgid "Close %1"
msgstr ""
