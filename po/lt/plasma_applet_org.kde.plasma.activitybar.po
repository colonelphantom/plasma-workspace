# Lithuanian translations for plasma-workspace package.
# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-14 01:39+0000\n"
"PO-Revision-Date: 2022-08-09 00:47+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: contents/ui/main.qml:71
#, kde-format
msgid "Switch to activity %1"
msgstr ""

#: contents/ui/main.qml:91
#, kde-format
msgctxt "@action:inmenu"
msgid "&Configure Activities…"
msgstr ""
